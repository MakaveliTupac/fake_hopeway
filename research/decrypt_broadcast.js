const dgram = require('dgram')
const crypto = require('crypto')

const a = Buffer.from('2A407362402A', 'hex');
const b = Buffer.from('646667686572677468626564676A35346A74366B6C3535333633357938396675673964666737383932336873666A6F3334746E6C646667697365663875', 'hex');

const header = Buffer.from('2A404446474A5552402A', 'hex')
const key = Buffer.from('676E3B6E677133347569796837754748262A4F232447247475687571696F68333437387479386F6740404C4845573A464A482A307570346A6839686867756973', 'hex');

const search = Buffer.from('{"msg_name":"SearchIpc","datetime":"2021-01-10 16:26:57"}');

const listen_port = 51507;
const broadcast_ip = '192.168.150.255';
const broadcast_port = 51537;

const socket = dgram.createSocket('udp4')
socket.bind(listen_port);

socket.on('listening', () => {
  socket.setBroadcast(true);
});

socket.on('message', async (packet) => {
  const obj = decrypt(packet);
  const { bind_flag, ipc_ip, ipc_id, user_key } = obj;
  if (!bind_flag) {
    console.log("WARNING: device appears unbound, this may not work");
  }
  if (user_key.length < 1) {
    console.log("WARNING: user_key is empty, this may not work");
  }

  const rtsp_pwd = crypto.createHash('md5').update(`eahgi5&$RUIHEDL#&C${ipc_id}${user_key}`).digest("hex")
  const rtsp_url = `rtsp://${ipc_ip}:554/${rtsp_pwd}_0`

  console.log({rtsp_pwd, rtsp_url})
})

socket.send(search, broadcast_port, broadcast_ip, (err, size) => {
  if (err) {
    console.log(err)
    return
  }
})


function decrypt(message) {
  if (Buffer.compare(message.slice(0, header.length), header) !== 0) {
    console.log(`Packet header ${message.slice(0, header.length)} didn't match expected`);
  }

  const clear = Buffer.alloc(message.length - header.length);
  for(var i = 0; i < message.length; i++) {
    clear[i] = message[i+header.length] ^ key[i % key.length];
  }

  const obj = JSON.parse(clear);
  return obj;
}
