# Hopeway/HZTCAM/M709 Camera

### Fixed strings

| string                | md5                              |
| --------------------- | -------------------------------- |
| 'hopeway6161'         | abd5acddce0f4a567206ce77a648fecf |
| 'eahgi5&\$RUIHEDL#&C' | 5066274cde4f99873bc1ba250fdd93c6 |
| 'hw1616'              | dc665de3d8c0eae51eb02022466be16c |
| 'GuE^T!#(\*dfgh58'    | c8fd7bd94aa09c6c7c15cff2225c9349 |

> echo -n 'GuE^T!#(\*dfgh58 123' | tr -d ' ' | md5

### Commands

| msg_index | msg_name            | notes                                                 |
| --------- | ------------------- | ----------------------------------------------------- |
| 1         | MotorControl        |                                                       |

| 3         | IpcRegister         | from device to cloud                              |
| 4         | IpcSwitch           |                                                       |
| 5 | StartSendStream | Cloud to device when app requests live |
| 6         |                     | `/app/stopSendStream`                                 |

| 8         | AudioTalkback       |                                                       |

| 10        | PreviewSdFile       |                                                       |
| 11        | StopPreviewSdFile   | |
| 12        | FormatSdCard        |                                                       |
| 13        | ConfigNetwork       |                                                       |
| 14        | SetIpcDateTime      |                                                       |
| 15        | GetIpcApInfo        | Scans nearby wifi APs. Name values are base64 encoded |
| 16        | GetIpcInfo          |                                                       |
| 17        | SetIpcPassword      |                                                       |
| 18        | VerifyIpcPassword   |                                                       |
| 19        | GetSdRecord         |                                                       |
| 20        | SetSdRecord         |                                                       |
| 21        | GetMonthRecordDay   |                                                       |
| 22        | GetDayRecordFile    |                                                       |
| 23 | IpcBind | From cloud to device when user puts password into app|
| 28        | LightModeControl    |                                                       |
| 29        | ChangeDomain        |                                                       |
| 34        | GetMdSwitch         |                                                       |
| 35        | SetMdSwitch         |                                                       |
| 36        | GetMotionAreaDetect |                                                       |
| 37        | SetMotionAreaDetect |                                                       |
| 38        | GetIntelligentConf  |                                                       |
| 39        | SetIntelligentConf  |                                                       |
| 40        | ClearAlarmVoice     |                                                       |
| 42        | SetMiniIpcLed       |                                                       |
| 43        | GetMiniIpcLed       |                                                       |
| 54        | GetIpcLanguage      |                                                       |
| 58        | SmartHomeSet        |                                                       |
| 60        | Ipc4gSet            |                                                       |
| 61        | Ipc4gGet            |                                                       |
| 62        | IpcCommCtrl         |                                                       |
|           | SearchIpc           | broadcast to find devices                             |

### GetMonthRecordDay

#### Request

```json
{
  "msg_index": 21,
  "msg_name": "GetMonthRecordDay",
  "year": 2021,
  "month": 1,
  "ipc_pwd": "5AFCAF7FB86E87CFCDAEA91485FE10AC",
  "from": 0,
  "ipc_id": "26300015641"
}
```

#### Response

```json
{
  "msg_name": "GetMonthRecordDay",
  "days": [1],
  "ret": 0,
  "ret_desc": "ok",
  "app_id": 49,
  "domain_num": 0
}
```

### GetDayRecordFile

#### Request

```
{"msg_index":22,"msg_name":"GetDayRecordFile","year":2021,"month":1,"day":1,"ipc_pwd":"5AFCAF7FB86E87CFCDAEA91485FE10AC","from":0,"ipc_id":"26300015641"}
```

#### Response

```json
{
  "msg_name": "GetDayRecordFile",
  "Files": [
    {
      "file": "00-01-10_2a.mkv",
      "size": 5691
    },
    {
      "file": "00-11-10_2a.mkv",
      "size": 5841
    },
    {
      "comment": "truncated list, you get the idea"
    },
    {
      "file": "18-42-40_2m.mkv",
      "size": 3053
    }
  ],
  "ret": 0,
  "ret_desc": "ok",
  "app_id": 49,
  "domain_num": 0
}
```

### Forum posts

#### Command pwd

I bought one of these and this is the only thread I've found that has any details on them. I'd prefer not to have to create an account for the app, so I'm working to be able to configure and stream without it. To that end, I'm trying to figure out the ipc_pwd and the rtsp url. So far I have found that the password is an md5 hash of a concatenation of a fixed string, sometimes the ipc id, and the user provided password (or '123' if factory fresh). The particular fixed string can vary depending on the ipc id and firmware version. It will be a challenge to figure out all the edge cases, but I can outline what I do have. Based on wireshark captures before I had set the password, the default was `e6d0987aa437d1c4f8177c683036938b`. I determined that this is the md5 of the string 'GuE^T!#(*dfgh58' plus the default password '123' ([code]echo -n 'GuE^T!#(*dfgh58 123' | md5[/code] on linux). I used the app to set a password and I chose a random word, "retire". The network traffic showed it changing to `5AFCAF7FB86E87CFCDAEA91485FE10AC`, which follows the pattern ([code]echo -n 'GuE^T!#(\*dfgh58retire' | md5[/code]). My Ipc Version is "0.1.1537-20201107", so this may work for others with the same version. I don't know a way to get the version without having the password, but you can see that the version includes a date, so I would guess this applies to new devices. The other fixed strings I found that are used to produce the ipc_pwd are '[spoiler]hopeway6161[/spoiler]', '[spoiler]eahgi5&\$RUIHEDL#&C[/spoiler]', and '[spoiler]hw1616[/spoiler]'.

I'm going to continue to work on getting an rtsp stream and I'll update if I find anything useful.

#### RTSP

I've made some headway with RTSP. From what I can gather, when the camera is connected to an access point as a client, the RTSP isn't available unless the camera is "bound", that is, associated to a cloud user. The command to do the binding is simple, but appears to only work if it comes via the device-cloud TCP connection (not the port 50513/554. I suspect this is also why the attempts to enable onvif didn't work.

When the camera is connected to an access point, it will attempt to connect to port 80 of 'service.houwei-tech.com'. It'll post some binary that start with 'hopeway' to '/device/register', and look for a json response. The json response has a 'pull_url' which will have service.houwei-tech.com's IP and one of 4 ports (9500, 9501, 9502, 9503). It will connect to that port, send another binary message that starts 'hopeway' (maybe the same one as was sent via http, I didn't check). It then starts sending heartbeats ('heartbeat1:<ipc_id>'). At this point the server can send the bind command which includes a 'user_key'. A fixed string ('eahgi5&\$RUIHEDL#&C'), ipc_id, and user_key are concatenated and md5 hashed to produce the path for the RTSP request (rtsp://<ip>:554/<md5 hash>\_0).

bind command: [code]{"msg_index":23,"msg_name":"IpcBind","bind_flag":true,"user_key":"131609452846781890","ipc_id":"26300015641","msg_no":"5fee986f98be4","from":1485359}[/code]

register response:[code]
{"ipc_datetime":"2021-01-02 03:53:27","msg_name":"IpcRegister","timezone":8,"cloud_url":"","cloud_package_index":0,"cloud_stream_type":1,"bcd_cloud":0,"cloud_md_always_record":true,"bind_flag":false,"user_key":"","pic_url":"http:\/\/service.houwei-tech.com\/device\/motionPic","ipc_uuid":"443649","ret":0,"ret_desc":"success","pull_url":"47.254.78.198:9501","verify":"5F13BF8B01FC64886C58643BDBEDD3EE","server_reserve":"3","upgrade_model":"","upgrade_version":"","upgrade_lfuv":"","upgrade_lfrc":"","upgrade_url":"","alarm_uri":"DEVICE_CALL_GATEWAY_HOST\/api\/DeviceAlarm\/alarmEventReport"}
[/code]

calculate streaming pwd:
[code]echo -n 'eahgi5&\$RUIHEDL#&C 26300015641 131609452846781890' | tr -d ' ' | md5[/code]

So what I did was configure my router to block the camera going to any DNS except the router's, add an entry for service.houwei-tech.com to point to a raspberry pi, and run a small server on ports 80 and 9500 of the pi. Port 80 returns the register response, but gives the pi's IP and port 9500 for the pull_url. Port 9500 ignores the binary message, responds to heartbeats with '{"msg_name":"Heartbeat","msg_index":2}', and sends the bind command. It got a success response and I was able to view the RTSP command. I even tried with a user_id of "0" in the bind command, and that worked as well with the updated streaming pwd.

I'm not sure if I needed to block DNS at my router. With 'GetIpcInfo' you can see both DNS servers: The camera showed my router as ipc_dns0, and '8.8.8.8' as ipc_dns1. My router was configured with 1.1.1.1 and 8.8.8.8, but even after changing 8.8.8.8 to something else I didn't see the change in the camera (tried turning it off/on, telling the router to make it reconnect, etc). It's possible I just needed to wait for the DHCP lease timeout, but who has patience for that? It is also possible the 8.8.8.8 is from the camera's firmware. At one point I turned off DHCP on the camera using the 'IpcSwitch' command and GetIpcInfo showed DNS of 114.114.114.114 and 8.8.8.8.

If there were a way to see the user_key in the app's UI, then it would be possible to calculate the streaming pwd and stream the video over RTSP. Personally I'd prefer a complete break to the vendor lock-in, which right now requires a bit of a high technical bar. With tooling similar to the tuya-convert project, I think this could be made easier, although still requiring a raspberry pi. They have scripts to create an access point on the pi with DHCP, DNS, and various servers. The user could use the app without an account to configure the camera to connect to that access point, and the binding to a fake user_key could be automated, just printing the rtsp url for the user. Actually, given that the camera runs an access point with a known name (IPC-...), it would probably be possible to automate configuring the wifi as well.

~The raspberry pi I wrote the server code on isn't configured to push to git, but I'll try to do that and push up my code later.~
https://gitlab.com/bettse/fake_hopeway
